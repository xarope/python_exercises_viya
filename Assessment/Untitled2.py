
# coding: utf-8

# In[21]:


import swat
import pandas as pd
from matplotlib import pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
swat.options.cas.print_messages = True


# In[92]:


# CAS(hostname,port,username,password)
conn = swat.CAS("server", 8777, "student", "Metadata0", protocol="http")


# In[117]:


# Change timeout
mytime = 60*60*12
conn.session.timeout(time=mytime)
conn.session.sessionStatus()


# In[93]:


conn.table.tableinfo()


# In[101]:


castbl = conn.CASTable(name="AMESHOUSING2")


# In[102]:


indata="Ameshousing2"


# In[103]:


castbl.head()


# In[28]:


display(castbl.mean())
castbl['SalePrice'].mean()


# In[29]:


conn.loadActionSet('regression')
actions = conn.builtins.help(actionSet='regression')


# In[104]:


# Get variable info and types
colinfo = conn.table.columninfo(table=indata)['ColumnInfo']
colinfo


# In[105]:


# Target variable is the first variable
target = colinfo['Column'][13]

# Get all variables
inputs = list(colinfo.query('Type=="double"')['Column'])
nominals = list(colinfo.query('Type=="char"')['Column'])

# Get only imputed variables
inputs.remove('SalePrice')

# Print                     
display(target)
display(inputs)
display(nominals)


# In[106]:


conn.loadActionSet('sampling')
actions = conn.builtins.help(actionSet='sampling')


# In[107]:


# Partition the data
conn.sampling.srs(
    table   = indata,
    samppct = 70,
    seed = 919,
    output  = dict(casOut = dict(name = indata, replace = True),  copyVars = 'ALL')
)


# In[109]:


# Refresh the castbl object
castbl = conn.CASTable(name="Ameshousing2")


# In[110]:


conn.loadActionSet('decisionTree')
actions = conn.builtins.help(actionSet='decisionTree')


# In[111]:


# Partition the data
conn.sampling.srs(
    table   = indata,
    samppct = 70,
    seed = 919,
    partind = True,
    output  = dict(casOut = dict(name = indata, replace = True),  copyVars = 'ALL')
)


# In[112]:


conn.loadActionSet('fedSql')
actions = conn.builtins.help(actionSet='fedSql')


# In[114]:


# Make sure the partition worked correctly using SQL
counts = conn.fedSql.execDirect(query =
    '''
    SELECT _PartInd_, count(*) 
    FROM Ameshousing2 
    GROUP BY _PartInd_;
    '''
)['Result Set']

display(counts)
counts['Percent'] = counts['COUNT']/sum(counts['COUNT'])
counts


# In[115]:


inputs2 = inputs + nominals


# In[121]:


conn.decisionTree.gbtreeTrain(
    table    = dict(name = indata, where = '_PartInd_ = 1'),
    target   = target, 
    inputs   = inputs2, 
    nominals = nominals,
    nTree    = 1000,
    casOut   = dict(name = 'gbt_model', replace = True),
    savestate = dict(name = 'gb_model', replace = True)
)


# In[118]:


conn.table.tableinfo()


# In[119]:


conn.loadActionSet('aStore')
actions = conn.builtins.help(actionSet='aStore')


# In[122]:


#Score the support vector machine model
conn.aStore.score(
    table    = dict(name = indata, where = '_PartInd_ = 0'),
    rstore = "gb_model",
    out = dict(name="gb_scored", replace=True)
)


# In[140]:


#Score the gradient boosting model
gb_score_obj = conn.decisionTree.gbtreeScore(
    table    = dict(name = indata, where = '_PartInd_ = 0'),
    model = "gbt_model",
    casout = dict(name="gbt_scored",replace=True),
    copyVars = target,
    encodename = True,
    assessonerow = True
)


# In[141]:


#View selected fields from the output object
display(gb_score_obj['OutputCasTables'])
gb_score_obj['ScoreInfo']


# In[142]:


conn.loadActionSet('percentile')
actions = conn.builtins.help(actionSet='percentile')


# In[143]:


gb_assess_obj = conn.percentile.assess(
   table = "gbt_scored",
   inputs = assess_input,
   casout = dict(name="gbt_assess",replace=True),
   response = target,
   event = "1"
)

